var colors = {
  light: [13, 13, 13],
  dark: [0, 0, 0]
}

var boardSize
var segments = []
var numOfSegments = 12
var intersections
var sw = 0.01
var es = 0.05
var frame = 0
var speed = 0.01

function setup() {
  if (windowWidth >= windowHeight) {
    boardSize = windowHeight - 80
  } else {
    boardSize = windowWidth - 80
  }
  createCanvas(windowWidth, windowHeight)

  for (var i = 0; i < numOfSegments; i++) {
    var p1, p2
    p1 = new Point(random(-0.4, 0.4), random(-0.4, 0.4))
    p2 = new Point(random(-0.4, 0.4), random(-0.4, 0.4))
    segments.push(new Segment(p1, p2))
  }

  intersections = calculateIntersections(segments)
}

function draw() {
  createCanvas(windowWidth, windowHeight)
  background(colors.light)
  rectMode(CENTER)
  colorMode(RGB, 255, 255, 255, 1)

  fill(colors.dark)
  noStroke()
  rect(windowWidth * 0.5, windowHeight * 0.5, boardSize, boardSize)

  for (var i = 0; i < segments.length; i++) {
    stroke(255)
    strokeCap(ROUND)
    strokeWeight(boardSize * sw)
    segments[i].display()
  }

  for (var i = 0; i < intersections.length; i++) {
    stroke(255)
    strokeWeight(boardSize * sw)
    fill((i % 2) * 255)
    intersections[i].display()
  }

  frame += speed * deltaTime
  if (frame >= 1) {
    frame = 0
    var p1, p2
    p1 = new Point(random(-0.4, 0.4), random(-0.4, 0.4))
    p2 = new Point(random(-0.4, 0.4), random(-0.4, 0.4))
    segments = segments.splice(1)
    segments.push(new Segment(p1, p2))
    for (var i = 0; i < segments.length; i++) {
      segments[i].start.x = segments[i].start.x + random(-0.01, 0.01)
      segments[i].start.y = segments[i].start.y + random(-0.01, 0.01)

      segments[i].end.x = segments[i].end.x + random(-0.01, 0.01)
      segments[i].end.y = segments[i].end.y + random(-0.01, 0.01)
    }
    intersections = calculateIntersections(segments)
  }
}

function calculateIntersections(segments) {
  var intersections = []
  var intersection
  var segment1
  var segment2
  for (var i = 0; i < numOfSegments; i++) {
    segment1 = segments[i]
    for (var j = i + 1; j < numOfSegments; j++) {
      segment2 = segments[j]
      intersection = interSection(segment1, segment2)
      if (intersection !== null) {
        intersections.push(intersection)
      }
    }
  }
  return intersections
}

function interSection(segment1, segment2) {
  var x1 = segment1.start.x
  var y1 = segment1.start.y
  var x2 = segment1.end.x
  var y2 = segment1.end.y
  var x3 = segment2.start.x
  var y3 = segment2.start.y
  var x4 = segment2.end.x
  var y4 = segment2.end.y

  var ua = (x4 - x3) * (y1 - y3) - (y4 - y3) * (x1 - x3)
  var denominator = (y4 - y3) * (x2 - x1) - (x4 - x3) * (y2 - y1)

  if (abs(denominator) < 0.0001) {
    return null
  }

  ua /= denominator
  if (ua < 0 || ua > 1) {
    return null
  }

  var ub = (x2 - x1) * (y1 - y3) - (y2 - y1) * (x1 - x3)
  ub /= denominator

  if (ub < 0 || ub > 1) {
    return null
  }

  var xi = x1 + ua * (x2 - x1)
  var yi = y1 + ua * (y2 - y1)

  return new Point(xi, yi)
}

class Point {
  constructor(x, y) {
    this.x = x
    this.y = y
  }

  display() {
    ellipse(windowWidth * 0.5 + boardSize * this.x, windowHeight * 0.5 + boardSize * this.y, boardSize * es)
  }
}

class Segment {
  constructor(start, end) {
    this.start = new Point(start.x, start.y)
    this.end = new Point(end.x, end.y)
  }

  display() {
    line(windowWidth * 0.5 + boardSize * this.start.x, windowHeight * 0.5 + boardSize * this.start.y, windowWidth * 0.5 + boardSize * this.end.x, windowHeight * 0.5 + boardSize * this.end.y)
  }
}

function windowResized() {
  if (windowWidth >= windowHeight) {
    boardSize = windowHeight - 80
  } else {
    boardSize = windowWidth - 80
  }
  createCanvas(windowWidth, windowHeight)
}
